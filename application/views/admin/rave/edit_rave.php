<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h4><i class="fa fa-pencil"></i> &nbsp; Edit Rave</h4>
        </div>
        <div class="col-md-6 text-right">
            <a href="<?= base_url('admin/rave'); ?>" class="btn btn-success"> Back</a>
        </div>
        <?php /*<div class="col-md-4 text-left">
          <a href="<?= base_url('admin/users'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Users List</a>
          <a href="<?= base_url('admin/users/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> Add New Invoice</a>
        </div> */?>
        
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open_multipart(base_url('admin/rave/edit_rave/'.$rave['id']), 'class="form-horizontal"' )?>
              <div class="form-group">
                <label for="description" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-9">
                    <textarea rows="8" name="description" class="form-control" id="description" placeholder="Description..."><?= $rave['description']; ?></textarea>
                  </div>
              </div>
              
              <div class="form-group">
                <label for="price" class="col-sm-2 control-label">Price</label>
                  <div class="col-sm-9">
                    <input type="number" name="price" value="<?= $rave['price']; ?>" class="form-control" id="price" placeholder="price">
                  </div>
              </div>
              
              <div class="form-group">
                <label for="video" class="col-sm-2 control-label">Icon Picture</label>
                <div class="col-sm-9">    
                   <input type="file" name="icon_picture" class="form-control" id="icon_picture" placeholder="" />
                </div>
              </div>
              
              <div class="form-group">
                <div class="col-md-11">
                  <input type="submit" name="submit" value="Update Rave" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section> 