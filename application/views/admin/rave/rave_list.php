<!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css"> 
  

 <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h3><i class="fa fa-list"></i> &nbsp; Rave List</h3>
        </div>
        <!--<div class="col-md-6 text-right">
          <div class="btn-group margin-bottom-20"> 
            <a href="<?= base_url('admin/users/create_users_pdf'); ?>" class="btn btn-success">Export as PDF</a>
            <a href="<?= base_url('admin/users/export_csv'); ?>" class="btn btn-success">Export as CSV</a>
          </div>
        </div>-->
        
      </div>
    </div>
  </div>
  
   <div class="box border-top-solid">
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
          <th>No</th>
          <th>Icon</th>
          <th>Name</th>
          <th>Mode</th>
          <th>Participants</th>
          <th>Location</th>
          <th>Feature</th>
          <th>Date</th>
          <th>Chargeable</th>
          <th>Price</th>
          <th>Description</th>
          <th>Visible</th>
          <th>Creator</th>
          <th>Created at:</th>
          <th>Updated at:</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
          <?php $i = 0;
          foreach($all_raves as $row): $i++; ?>
          <tr>
            <td><?= $i; ?></td> 
            <td ><img src="<?= $row['icon_url']; ?>" style="background-color: gray; width: 60px; height: 50px;" ></td>
            <td><?= $row['name']; ?></td>
            <td><?= ucfirst($row['mode']); ?></td>
            <td><?= $row['participants'].' / '.$row['participant_capacity']; ?></td>
            <td><?= $row['location_name']; ?></td>
            <td><?= $row['feature']; ?></td>
            <td><?= $row['date']; ?></td>
            <td class="text-center">
                
                <a href="<?= base_url('admin/rave/update_chargeable/'.$row['id']); ?>" 
                <?= ($row['is_chargeable'] == 'yes')? 'class="btn btn-success btn-flat btn-xs"': 'class="btn btn-info btn-flat btn-xs"' ?>>
                <?= ($row['is_chargeable'] == 'yes')? 'Chargeable': 'Free' ?>
                </a>
               
            </td>
            <td><?= $row['price']; ?></td>
            <td><?= $row['description']; ?></td>
            <td> 
               <? if ($row['allow_hide'] == 'yes') { ?>
                <a href="<?= ($row['allow_hide'] == 'yes') ? base_url('admin/rave/update_visible/'.$row['id']) : '#'?>"
                <?= ($row['is_visible'] == 'yes')? 'class="btn btn-success btn-flat btn-xs"': 'class="btn btn-info btn-flat btn-xs"' ?>>
                <?= ($row['is_visible'] == 'yes')? 'Visible': 'Invisible' ?>
                </a>
               <? } else { ;
                  ($row['is_visible'] == 'yes')? 'class="btn btn-success btn-flat btn-xs"': 'class="btn btn-info btn-flat btn-xs"'. 
                  ($row['is_visible'] == 'yes')? 'Visible': 'Invisible'; 
               } ?>           
            </td>
            <td><?= $row['owner_name']; ?></td>
            <td><?= explode(' ', $row['created_at'])[0]; ?></td>
            <td><?= explode(' ', $row['updated_at'])[0]; ?></td>
            <td class="text-center">                        
              <a href="<?= ($row['allow_edit'] == 'yes') ? base_url('admin/rave/edit_rave/'.$row['id']) : '#' ?>" class="btn btn-warning btn-flat btn-xs">Edit</a>              
            </td>            
          </tr>
          <?php endforeach; ?>
        </tbody>
       
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>  


<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Dialog</h4>
      </div>
      <div class="modal-body">
        <p>As you sure you want to delete.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a class="btn btn-danger btn-ok">Yes</a>
      </div>
    </div>

  </div>
</div>


<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script> 
<script type="text/javascript">
  $('#confirm-delete').on('show.bs.modal', function(e) {
  $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
});
</script>
  
<script>
$("#view_raves").addClass('active');
$(document).on("click","#link",function(){
 alert("I am a pop up ! ");
});
</script>

   