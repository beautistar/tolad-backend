<!DOCTYPE html>
<html lang="en">
    <head>
          <title><?=isset($title)?$title: APP_NAME ?></title>
          <!-- Tell the browser to be responsive to screen width -->
          <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
          <!-- Bootstrap 3.3.6 -->
          <link rel="stylesheet" href="<?= base_url() ?>public/bootstrap/css/bootstrap.min.css">
          <!-- Theme style -->
          <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/AdminLTE.min.css">
           <!-- Custom CSS -->
          <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/style.css">
           <!-- jQuery 2.2.3 -->
          <script src="<?= base_url() ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
       
    </head>
    <body style="">

        <div class="container">
            <div class="row">
                
                <div class="col-md-4 col-md-offset-4 text-center">
                    <div class="login-title">
                        <h3><span><?= APP_NAME?></span></h3>
                    </div>
                    <?php if($verifytoken == 1) { ?>
           
            
                <?php echo form_open(base_url('api/user/forgot'), 'class="form-horizontal"');  ?>
                <!--<form class="form-horizontal" role="form" method="post">-->
                    <h2>Reset Your Password</h2>
                    <?php if (isset($userid)) { ?>
                    <input type="hidden" name="userid" value="<?=$userid;?>">
                    <?php } ?>
                    <?php if (isset($token)) { ?>
                    <input type="hidden" name="token" value="<?=$token;?>">
                    <?php } ?>                     
                    
                    <?php if(isset($msg)) { ?>
                        <div class="<?php echo $msgclass; ?>" style="padding:5px;"><?php echo $msg; ?></div>
                    <?php if ($msgclass == 'bg-success') echo form_close( ); return;} ?>
        
                    <div class="row">
                        <div class="col-lg-12">
                        <label class="control-label">New Password</label>
                        </div>
                    </div>
        
                    <div class="row">
                        <div class="col-lg-12">
                            <input class="form-control" name="new_password" type="password" placeholder="New Password" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                        <label class="control-label">Re-type New Password</label>
                        </div>
                    </div>
        
                    <div class="row">
                        <div class="col-lg-12">
                            <input class="form-control" name="retype_password" type="password" placeholder="Re-type New Password" required>
                        </div>
                    </div>
        
                    <div class="row">
                        <div class="col-lg-12">
                            <button class="btn btn-success btn-block" name="submit" style="margin-top:8px;">Submit</button>
                        </div>
                    </div>
                <!--</form>-->
                <?php echo form_close( ); ?>
            
            
            <?php }else if ($verifytoken == 0){?>
                <div >
                <h2>Invalid or Broken Token</h2>
                    <p>Opps! The link you have come with is maybe broken or already used. Please make sure that you copied the link correctly or request another token from <a href="index.php">here</a>.</p>
                </div>
            <?php }?>

                </div> 
            </div> 
        </div>
    </body>

    <!-- Bootstrap 3.3.6 -->
    <script src="<?= base_url() ?>public/bootstrap/js/bootstrap.min.js"></script>
    
    <!-- AdminLTE App -->
    <script src="<?= base_url() ?>public/dist/js/app.min.js"></script>
  

    <style type="text/css">
    
    body{
        background-color: white;
        background-position: center;
        background-size: cover;
     }
    .login-title{
        padding-top: 80px;
        color: #fff;
    }
    .login-title span{
        font-size: 30px;
        line-height: 1.9;
        display: block;
        font-weight: 700;
    }
    .form-box{
        position: relative;
        background: #ffffffa6;
        max-width: 375px;
        width: 100%;
        box-shadow: 0 0 3px rgba(0, 0, 0, 0.1);
        padding: 51px 20px 41px 20px;
    }

  
    .form-control{
        background: #ffffffd6;
    }
    .login-form input[type=text], .login-form input[type=password]{
        margin-bottom:10px;
    }

    .login-form input {
        outline: none;
        display: block;
        width: 100%;
        border: 1px solid #d9d9d9;
        margin: 0 0 20px;
        padding: 10px 15px;
        box-sizing: border-box;
        border-radius: 0;
        -moz-border-radius: 0;
        -webkit-border-radius: 0;
        min-height: 40px;
        font-wieght: 400;
        -webkit-transition: 0.3s ease;
        transition: 0.3s ease;
    }

    .login-form input[type=submit]{
        cursor: pointer;
        background: #33b5e5;
        width: 100%;
        border: 0;
        padding: 8px 15px;
        color: #ffffff;
        -webkit-transition: 0.3s ease;
        transition: 0.3s ease;
        border-radius: 0;
        -moz-border-radius: 0;
        -webkit-border-radius: 0;
        min-height: 40px;
        
    }
    </style>
</html>