<?php
  
class User_model extends CI_Model {
    
    public function login($data){
        
        $this->db->where('username', $data['username']);
        $this->db->or_where('email', $data['username']);
        $query = $this->db->get('user');
        
        if ($query->num_rows() == 0){
            return false;
        }
        else{
            //Compare the password attempt with the password we have stored.
            $result = $query->row_array();
            $validPassword = password_verify($data['password'], $result['password']);
            if($validPassword){
                return $result = $query->row_array();
            }            
        }
    }
    
    function check_exist($username) {        
        
        $query = $this->db->where('username', $username);
        $query = $this->db->get('user');
        
        if ($query->num_rows() == 0) {
            
            return false;
        } else {
            
            return true;
        }
    }
    
    
    public function add_user($data){
        
        $this->db->insert('user', $data);
        return $this->db->insert_id();
    }

    function get_profile($id) {

        $this->db->where('id', $id);
        return $this->db->get('user')->row_array()['photo_url'];
    }
    
    function edit_user($id, $data) {
        
        $this->db->where('id', $id);
        $this->db->update('user', $data);
    }
    
    function update_photo($id, $data) {
        
        $this->db->where('id', $id);
        $this->db->update('user', $data);        
    }  
    
    function get_user($user_id) {
        
        $this->db->where('id', $user_id);
        return $this->db->get('user')->row_array();
    }
    
    function get_user_by_qrcode($qr_code) {
        
        $this->db->where('qr_code', $qr_code);
        $query = $this->db->get('user');
        
        if ($query->num_rows() == 0){
            return false;
        
        } else {

            return $result = $query->row_array();
            
        }            
        
    }    
    
    function add_friend($data) {
        
        $query = $this->db->where($data)->get('friend');
        
        if ($query->num_rows() == 0) {
        
            $this->db->insert('friend', $data);
            return $this->db->insert_id();      
        } else {
            return false;
        }
        
    }
    
    function get_freinds($user_id) {
        
        return $this->db->select('A.id, A.name, A.surname, A.email, A.phone, A.birthday,
                                 A.photo_url, A.qr_code')
                 ->from('friend as B')
                 ->where('A.id !=', $user_id)
                 ->where('B.user_id', $user_id)
                 ->or_where('B.friend_id', $user_id)
                 ->join('user as A', 'A.id = B.user_id')
                 ->get()
                 ->result_array();        
    }
    
    
    function get_users($user_id) {
        
        $this->db->where('id !=',$user_id);
        return $this->db->get('user')->result_array();
    }
    
    function check_exist_email($email) {        
        
        $query = $this->db->where('email', $email);
        $query = $this->db->get('user');
        
        if ($query->num_rows() == 0) {
            
            return false;
        } else {
            
            return true;
        }
    }
    function getUserIdByEmail($email) {
        
        return $this->db->where('email', $email)->get('user')->row()->id;
    }
    
    function insert_key($user_id, $token) {
        
        $this->db->set('userID', $user_id);
        $this->db->set('token', $token);
        $this->db->insert('recovery_keys');
    }
    
    function verify_token($user_id, $token) {
        
        $this->db->where('userID', $user_id);
        $this->db->where('token', $token);
        return $this->db->get('recovery_keys')->row_array();
    }
    
    function update_password($user_id, $new_password) {
        
        $this->db->where('id', $user_id);
        $this->db->set('password', $new_password);
        $this->db->update('user');
        return true;
    }
    
    function update_recovery_key($user_id, $token) {
        
        $this->db->where('userID', $user_id);
        $this->db->where('token', $token);
        $this->db->set('valid', 0);
        $this->db->update('recovery_keys');
        
        
    }

    
    function delete_user($user_id) {
        
        $this->db->where('id', $user_id);
        $this->db->delete('user');
    }
}
?>
