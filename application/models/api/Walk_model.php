<?php

class Walk_model extends CI_Model {
    
    function add_rave($data) {
        
        $this->db->insert('walk', $data);
        return $this->db->insert_id();
    }
    
    function get_walks($user_id) {
        
        $query = $this->db->get_where('walk', array('user_id' => $user_id));
        
        //if ($query->row()->bg_image_id == 0) {
            
            return $query->result_array();
        
        //} else {

        //    return $this->db->select('A.id, A.user_id, A.profile_url, A.deceased_name, A.birth_date, 
        //                              A.about_person, A.created_at, A.updated_at, B.background_url as background_url')
        //         ->from('walk as A')
        //         ->where('A.user_id', $user_id)
        //         ->join('background as B', 'A.bg_image_id = B.id')
        //         ->get()
        //         ->result_array();
        //}        
    }

    function add_location($data) {
            
        $this->db->insert('location', $data);
        return TRUE;
    }

    function get_location($walk_id) {

        
        $this->db->where('walk_id', $walk_id);
        $this->db->order_by('id', 'ASC');
        return $this->db->get('location')->result_array();

    }
    
    function edit_walk($walk_id, $data) {

        $this->db->where('id', $walk_id);
        $this->db->update('walk', $data);
        
    }

    function add_bg_image($data) {

        $this->db->insert('background', $data);
        return $this->db->insert_id();


    }

    function get_background_image() {

        return $this->db->get('background')->result_array();
    }

    function get_background_url($bg_image_id) {

        return $this->db->where('id', $bg_image_id)
                    ->get('background')
                    ->row()
                    ->background_url;
    }

    function delete_walk($walk_id) {

        $this->db->where('id', $walk_id);
        $this->db->delete('walk');

        $this->db->where('walk_id', $walk_id);
        $this->db->delete('location');

    }
    
    function delete_walk_by_user($user_id) {

        $this->db->where('user_id', $user_id);
        $this->db->delete('walk'); 

    }
}
?>
