<?php
    class Template_model extends CI_Model{

        
        public function get_all_templates() {
            
            return $this->db->get('template')->result_array();
            
        }
        
        public function add_template($data){
            $this->db->insert('template', $data);
            return true;
        }
        
        public function get_template_by_id($id) {
            
            return $this->db->where('id', $id)->get('template')->row_array();
        }
        
        function update_template($id, $data) {
            
            $this->db->where('id', $id);
            return $this->db->update('template', $data);
        } 
      
    }

?>