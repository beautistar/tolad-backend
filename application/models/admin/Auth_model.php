<?php
	class Auth_model extends CI_Model{

		public function login($data){
			$query = $this->db->get_where('admin', array('email' => $data['email']));
			if ($query->num_rows() == 0){
				return false;
			}
			else{
				//Compare the password attempt with the password we have stored.
				$result = $query->row_array();
			    $validPassword = password_verify($data['password'], $result['password']);
			    if($validPassword){
			        return $result = $query->row_array();
			    }
				
			}
		}
		
		public function get_admin_detail(){
			$id = $this->session->userdata('admin_id');
			$query = $this->db->get_where('admin', array('id' => $id));
			return $result = $query->row_array();
		}

		public function update_admin($data){
			$id = $this->session->userdata('admin_id');
			$this->db->where('id', $id);
			$this->db->update('admin', $data);
			return true;
		}

		public function change_pwd($data, $id){
			$this->db->where('id', $id);
			$this->db->update('admin', $data);
			return true;
		}
        
        public function add_admin($data) {
            $this->db->insert('admin', $data);
            return true;
        }

	}

?>