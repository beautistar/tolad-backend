<?php
    class Rave_model extends CI_Model{

        
        public function get_all_rave() {
            
            return $this->db->get('rave')
                            ->result_array();
            
        }
        
        function update_visible($id) {
            
            $visible = $this->db->where('id', $id)->get('rave')->row()->is_visible;
            
            $visible = $visible == 'yes' ? 'no' : 'yes';
            
            $this->db->where('id', $id);
            $this->db->set('is_visible', $visible);
            $this->db->update('rave');
        }
        
        function update_chargeable($id) {
            
            $chargeable = $this->db->where('id', $id)->get('rave')->row()->is_chargeable;
            
            $chargeable = $chargeable == 'yes' ? 'no' : 'yes';
            
            $this->db->where('id', $id);
            $this->db->set('is_chargeable', $chargeable);
            $this->db->update('rave');
            
            
        }
        
        function get_rave_one($rave_id) {
            
            return $this->db->where('id', $rave_id)->get('rave')->row_array();
        }
        
        function edit_rave($rave_id, $data) {
            
            $this->db->where('id', $rave_id);
            $this->db->update('rave', $data);
        }        
    }

?>