<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Template extends MY_Controller {

        public function __construct(){
            parent::__construct();
            $this->load->model('admin/template_model', 'template_model');
        }

        //----------------------------------------------------------------------
        //  templates
        public function index(){
            $data['all_template'] =  $this->template_model->get_all_templates();
            $data['title'] = 'Templates';
            $data['view'] = 'admin/template/template_list';
            $this->load->view('admin/layout', $data);
        }
        
        
        //---------------------------------------------------------------
        //  Add template
        public function add(){
            
            if($this->input->post('submit')){

                $this->form_validation->set_rules('name', 'Name', 'trim|required|is_unique[template.name]'); 
                $this->form_validation->set_rules('participant_capacity', 'Participant amount', 'trim|required'); 
                $this->form_validation->set_rules('mode', 'Mode', 'trim|required'); 
                $this->form_validation->set_rules('is_chargeable', 'Chargeable', 'trim|required');
                
                if ($this->form_validation->run() == FALSE) {
                    $data['title'] = 'Add Template';
                    $data['view'] = 'admin/template/add_template';
                    $this->load->view('admin/layout', $data);
                }
                else{
                    $data = array(
                        'name' => $this->input->post('name'),
                        'participant_capacity' => $this->input->post('participant_capacity'),
                        'mode' => $this->input->post('mode'),
                        'is_chargeable' => $this->input->post('is_chargeable'),
                        'created_at' => date('Y-m-d h:m:s')
                    );
                    $result = $this->template_model->add_template($data);
                    if($result){
                        $this->session->set_flashdata('msg', 'Template has been added Successfully!');
                        redirect(base_url('admin/template'));
                    }
                }
            }
            else {
                
                $data['title'] = 'Add Template';
                $data['view'] = 'admin/template/add_template';
                $this->load->view('admin/layout', $data); 
            }                       
        }
        
        //---------------------------------------------------------------
        //  Edit Template
        public function edit($id = 0){
            if($this->input->post('submit')){
                $this->form_validation->set_rules('name', 'Name', 'trim|required'); 
                $this->form_validation->set_rules('participant_capacity', 'Participant amount', 'trim|required'); 
                $this->form_validation->set_rules('mode', 'Mode', 'trim|required'); 
                $this->form_validation->set_rules('is_chargeable', 'Chargeable', 'trim|required');

                if ($this->form_validation->run() == FALSE) {
                    $data['template'] = $this->template_model->get_template_by_id($id);
                    $data['title'] = 'Edit Template';
                    $data['view'] = 'admin/template/edit_template';
                    $this->load->view('admin/layout', $data);
                }
                else{
                    $data = array(
                        'name' => $this->input->post('name'),
                        'participant_capacity' => $this->input->post('participant_capacity'),
                        'mode' => $this->input->post('mode'),
                        'is_chargeable' => $this->input->post('is_chargeable'),
                        'created_at' => date('Y-m-d h:m:s')
                    );
                    $result = $this->template_model->update_template($id, $data);
                    if($result){
                        $this->session->set_flashdata('msg', 'Template has been updated Successfully!');
                        redirect(base_url('admin/template'));
                    }
                }
            }
            else{
                $data['template'] = $this->template_model->get_template_by_id($id);
                $data['title'] = 'Edit Template';
                $data['view'] = 'admin/template/edit_template';
                $this->load->view('admin/layout', $data);
            }
        }

        
        
        //----------------------------------------------------------------------
        //  delete template
        public function del_template($id = 0){
            $this->db->delete('template', array('id' => $id));
            $this->session->set_flashdata('msg', 'Template has been Deleted Successfully!');
            redirect(base_url('admin/template'));
        } 
    }


?>
