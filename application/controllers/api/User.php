<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {     

    function __construct(){

        parent::__construct();
        $this->load->database();
        $this->load->model('api/user_model', 'user_model');
        $this->load->model('api/walk_model', 'walk_model');
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

    private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
    }

    /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

    private function doRespondSuccess($result){

        $result['message'] = "Success.";
        $this->doRespond(200, $result);
    }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

    private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
    }
    
    function version() {
        phpinfo();
    }
    
    function generateRandomString($length = 20) {
    // This function has taken from stackoverflow.com
    
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return md5($randomString);
    }
    
    function send_mail($email, $token) {
        
        $to = $email;
        $subject = "Tolad Reset password";
        $header = "Tolad"; 
        $header .= "Content-Type: text/html; charset=UTF-8\r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $link = "http://18.196.178.198/api/user/forgot?email=$email&token=$token";
        $body = "Hello\nYou have requested for your password recovery\n";
        $body .= "Please click below link to reset your password.\n";
        $body .= $link;
        $sent=mail($to,$subject,$body,$header);
        
        if ($sent) {
            return "success";
        } else {
            return "false";
        }
    }       
    
    function login() {
        
        $data = array(
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password')
                );
        $result = $this->user_model->login($data);
        
        if ($result == TRUE) {
            $user_data['user_data'] = array(
                'id' => $result['id'],
                'username' => $result['username'],
                'email' => $result['email'],
                'photo_url' => $result['photo_url'],
            );
            
            $this->doRespondSuccess($user_data);
        }
        else{
            
            $message = "Invalid login";
            $this->doRespond(201, array('message' => $message));
        }
    } 
        
    function register() {
        
        $result = array();
        
        $exist = $this->user_model->check_exist($this->input->post('username'));
        
        if ($exist == TRUE) {
            
            $response['message'] = "User already eixst.";
            $this->doRespond(202, $response);
        
        } else {
            
            $data = array(
                      'login_type' => $this->input->post('login_type'),
                      'username' => $this->input->post('username'),
                      'email' => $this->input->post('email'),
                      'photo_url' => $this->input->post('photo_url'),
                      'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
                      'created_at' => date('Y-m-d h:m:s'),                           
                  );
            
            $id = $this->user_model->add_user($data);
            $user_data['user_data'] = array(
                'id' => $id,
                'username' => $data['username'],
                'email' => $data['email'],
                'photo_url' => $this->user_model->get_profile($id),
            ); 
                        
            $this->doRespondSuccess($user_data);   
        }                                               
    }   
    
    // upload user profile photo
    function upload_photo() {
        
        $id = $this->input->post('user_id');
        
        if(!is_dir("uploadfiles/")) {
            mkdir("uploadfiles/");
        }
        $upload_path = "uploadfiles/";  

        $cur_time = time();
         
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
         
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }
         
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 4000,
            'max_height' => 4000,
            'file_name' => $dateY.$dateM.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('photo')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array(
                          'photo_url' => $file_url, 
                          'updated_at' => date('Y-m-d h:m:s')                                                     
                      );
            $id = $this->user_model->update_photo($id, $data);
            $result['photo_url'] = $file_url;            
                      
            $this->doRespondSuccess($result);

        } else {
            $result['message'] = "Photo upload faied.";
            $this->doRespond(203, $result);
            return;
        }
    }
    
    /**
    * get post owner profile information
    * 
    */
    function get_user_profile() {
        
        $result = array();
        
        $user_id = $this->input->post('user_id');
        
        $user_data = $this->user_model->get_user($user_id);
        
        $result['user_data'] = array(
                'id' => $user_data['id'],
                'username' => $user_data['username'],
                'email' => $user_data['email'],
                'photo_url' => $user_data['photo_url'],
            );
        
        //$posts = $this->media_model->get_user_media($user_id);
            
        $result['post_data'] = $posts;        
            
        $this->doRespondSuccess($result);        
        
    }
    
    
    function check_user() {
        
        $email = $this->input->post('email');
        $exist = $this->user_model->check_exist_email($email);
        
        if ($exist == TRUE) {
            
            $response['message'] = "User is eixst.";
            $this->doRespond(200, $response);
        
        } else {
            $response['message'] = "User does not eixst.";
            $this->doRespond(204, $response);
            
        }        
    }
    
    function forgot_password() {
        
        $email = $this->input->post('email');
        $exist = $this->user_model->check_exist_email($email);
        if ($exist == TRUE) {
            
            $user_id = $this->user_model->getUserIdByEmail($email);
            $token = $this->generateRandomString();
            $this->user_model->insert_key($user_id, $token);
            $send_mail = $this->send_mail($email, $token);
            if ($send_mail === "success") {
                
                $response['message'] = 'A mail with recovery instruction has sent to your email. Please check your mail inbox or spam or junk';
                $this->doRespond(200, $response);
                
            } else {
                $msg = 'There is something wrong.';
                $msgclass = 'bg-danger';
                $response['message'] = 'There is something wrong.';
                $this->doRespond(205, $response);
            }
        
        } else {
            
            $response['message'] = "User does not eixst.";
            $this->doRespond(204, $response);             
        }
        
    }
    
    function forgot() {
        
        if(isset($_POST['submit'])) {
            $new_password = $_POST['new_password'];
            $retype_password = $_POST['retype_password'];
            $user_id = $_POST['userid'];
            $token = $_POST['token'];
            
            if($new_password == $retype_password) {
                $new_password = password_hash($new_password, PASSWORD_BCRYPT);
                $update_password = $this->user_model->update_password($user_id, $new_password);
                if($update_password){                    
                    
                    $data['verifytoken'] = 1;
                    $data['userid'] = $user_id;
                    $data['token'] = $token;
                    $this->user_model->update_recovery_key($user_id, $token);
                    $data['msg'] = 'Your password has changed successfully. Please login with your new passowrd.';
                    $data['msgclass'] = 'bg-success';
                    $this->load->view('admin/auth/reset_pwd', $data);
                }
            } else {
                $data['verifytoken'] = 1;
                $data['userid'] = $user_id;
                $data['token'] = $token;
                $data['msg'] = "Password doesn't match";
                $data['msgclass'] = 'bg-danger';
                $this->load->view('admin/auth/reset_pwd', $data);
               
            }
            
        } else {
            $email = $_GET['email'];
            $token = $_GET['token'];
            $user_id = $this->user_model->getUserIdByEmail($email);
            $row_query = $this->user_model->verify_token($user_id, $token);
            $verify_token = 0;
            if (count($row_query) > 0 ) {

                if($row_query['valid'] == 1) {
                    $verify_token = 1;                 
                    } 
            }
            $data['verifytoken'] = $verify_token;
            $data['userid'] = $user_id;
            $data['token'] = $token;
            $this->load->view('admin/auth/reset_pwd', $data);
        }
    }   
    
    function edit_profile() {
        
        $result = array();
        
        $user_id = $this->input->post('user_id');              
        $data = array(                       
                  'username' => $this->input->post('username'),
                  'email' => $this->input->post('email'),
                  'photo_url' => $this->input->post('photo_url'),
                  'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
                  'updated_at' => date('Y-m-d h:m:s')
                                             
              );
        
        $this->user_model->edit_user($user_id, $data);    
        
        $this->doRespondSuccess($result);   
        
    }
    
    function delete_profile() {
        
        $result = array();
        
        $user_id = $this->input->post('user_id'); 
        
        $this->user_model->delete_user($user_id);
        $this->walk_model->delete_walk_by_user($user_id);
        
        $this->doRespondSuccess($result);
        
        
        
    }
        
        
                                   
    
}
?>
