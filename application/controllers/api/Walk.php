<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Walk extends CI_Controller {     

    function __construct(){

        parent::__construct();         
        $this->load->database();
        $this->load->model('api/walk_model', 'walk_model');             
    }
    
    private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
    }

    /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

    private function doRespondSuccess($result){

        $result['message'] = "Success.";
        $this->doRespond(200, $result);
    }

    
    
    function create_walk() {
        
        $result = array();
        
        $user_id = $this->input->post('user_id');
        $deceased_name = $this->input->post('deceased_name');
        $birth_date = $this->input->post('birth_date');
        $passing_date = $this->input->post('passing_date');
        $about_person = $this->input->post('about_person');
        $bg_image_id = $this->input->post('bg_image_id');
        
        if(!is_dir("uploadfiles/")) {
            mkdir("uploadfiles/");
        }
        $upload_path = "uploadfiles/";  

        $cur_time = time();
         
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
         
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }
         
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;

        // Upload file.         
        $background_url = "";
        $profile_url = "";
        $uploadMainTo = null;
        if(isset($_FILES['profile_image'])){
            $profile_image_name = $_FILES['profile_image']['name'];
            $profile_image_size = $_FILES['profile_image']['size'];
            $profile_image_tmp = $_FILES['profile_image']['tmp_name'];
            $uploadMainTo = $upload_path.$profile_image_name;
            $moveMain = move_uploaded_file($profile_image_tmp,$uploadMainTo);

            $profile_url = $upload_url.$profile_image_name;
        }

        $uploadSecondTo = null;
        if(isset($_FILES['background_image'])){
            $background_image_name = $_FILES['background_image']['name'];
            $background_image_size = $_FILES['background_image']['size'];
            $background_image_tmp = $_FILES['background_image']['tmp_name'];
            $uploadSecondTo = $upload_path.$background_image_name;
            $moveSecond = move_uploaded_file($background_image_tmp,$uploadSecondTo);

            $background_url = $upload_url.$background_image_name;
        }
        
        $data = array('user_id' => $user_id,
                  'deceased_name' => $deceased_name,
                  'birth_date' => $birth_date,
                  'passing_date' => $passing_date,
                  'about_person' => $about_person,
                  'profile_url' => $profile_url,
                  'background_url' => $background_url,
                  'bg_image_id' => $bg_image_id,
                  'created_at' => date('Y-m-d h:m:s')                                                     
                  );
            
        $result['walk_id'] = $this->walk_model->add_rave($data);             
        $result['profile_url'] = $profile_url;
        $result['background_url'] = $background_url;
        $this->doRespondSuccess($result);
    }

    // get walks
    function get_walks() {

        $result = array();
        $t_result = array();
        
        $user_id = $this->input->post('user_id');
        
        $model_result =  $this->walk_model->get_walks($user_id);

        foreach ($model_result as $row) {

            if ($row['bg_image_id'] != 0) {

                $row['background_url'] = $this->walk_model->get_background_url($row['bg_image_id']);
            }

            array_push($t_result, $row);
        }

        $result['walk_list'] = $t_result;

        $this->doRespondSuccess($result);
    }

    // add location
    
    // function add_location() {
        
    //     $data = array('walk_id' => $this->input->post('walk_id'),
    //                   'latitude' => $this->input->post('latitude'),
    //                   'longitude' => $this->input->post('longitude'),
    //                   'created_at' => date('Y-m-d h:m:s'));
        
    //     $this->walk_model->add_location($data);
    //     $this->doRespondSuccess(array());
    // }

    // add location

    function add_location() {

        $data = json_decode(file_get_contents('php://input'), true);

        $locations = $data["locations"]; 
        
        $walk_id = $data['walk_id'];
        $walk_number = $data['walk_number'];
        
        foreach($locations as $location) {            

            $field = array('walk_id' => $walk_id,
                          'latitude' => $location['latitude'],
                          'longitude' => $location['longitude'],
                          'walk_number' => $walk_number,
                          //'created_at' => date('Y-m-d h:m:s')
                          );
            $this->walk_model->add_location($field);
        }

        $this->doRespondSuccess(array());
    }

    // get location in specific walk

    function get_location() {

        $walk_id = $this->input->post('walk_id');

        $result['location_list'] = $this->walk_model->get_location($walk_id);
        $this->doRespondSuccess($result);
    }
    
    // edit walk
    function edit_walk() {
        
        $result = array();
        
        $walk_id = $this->input->post('walk_id');
        $user_id = $this->input->post('user_id');
        $deceased_name = $this->input->post('deceased_name');
        $birth_date = $this->input->post('birth_date');
        $passing_date = $this->input->post('passing_date');
        $about_person = $this->input->post('about_person');
        $bg_image_id = $this->input->post('bg_image_id');

        $data = array('user_id' => $user_id,
                  'deceased_name' => $deceased_name,
                  'birth_date' => $birth_date,
                  'passing_date' => $passing_date,
                  'about_person' => $about_person,   
                  'bg_image_id' => $bg_image_id,               
                  'updated_at' => date('Y-m-d h:m:s')                                                     
                  );

        if(!is_dir("uploadfiles/")) {
            mkdir("uploadfiles/");
        }
        $upload_path = "uploadfiles/";  

        $cur_time = time();
         
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
         
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }
         
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;
        
        // Upload file.         
        $background_url = "";
        $profile_url = "";
        $uploadMainTo = null;
        if(isset($_FILES['profile_image'])){
            $profile_image_name = $_FILES['profile_image']['name'];
            $profile_image_size = $_FILES['profile_image']['size'];
            $profile_image_tmp = $_FILES['profile_image']['tmp_name'];
            $uploadMainTo = $upload_path.$profile_image_name;
            $moveMain = move_uploaded_file($profile_image_tmp,$uploadMainTo);

            $profile_url = $upload_url.$profile_image_name;
            $data['profile_url'] = $profile_url;
        }

        $uploadSecondTo = null;
        if(isset($_FILES['background_image'])){
            $background_image_name = $_FILES['background_image']['name'];
            $background_image_size = $_FILES['background_image']['size'];
            $background_image_tmp = $_FILES['background_image']['tmp_name'];
            $uploadSecondTo = $upload_path.$background_image_name;
            $moveSecond = move_uploaded_file($background_image_tmp,$uploadSecondTo);

            $background_url = $upload_url.$background_image_name;
            $data['background_url'] = $background_url;
        }
            
        $this->walk_model->edit_walk($walk_id, $data);            
        
        $this->doRespondSuccess($data);
    }

    function upload_background() {
        
        $result = array();
        
        if(!is_dir("uploadfiles/bg/")) {
            mkdir("uploadfiles/bg/");
        }
        $upload_path = "uploadfiles/bg/";  

        $cur_time = time();
         
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
         
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }
         
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 4000,
            'max_height' => 4000,
            'file_name' => $dateY.$dateM.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig); 
        
        if ($this->upload->do_upload('background_image')) {
            
            $background_url = $w_uploadConfig['upload_url'].$this->upload->file_name; 
            $data = array(
                  'background_url' => $background_url,                  
                  'created_at' => date('Y-m-d h:m:s')                                                     
                  );
            $this->walk_model->add_bg_image($data);                
        
        }else {
            $result['message'] = "Failed with backgroud picture upload.";
            $this->doRespond(203, $result);
            return;
        }

        $result['background_url'] = $background_url;
        $this->doRespondSuccess($result);
    }

    function get_background_image() {

        $result['background_images'] = $this->walk_model->get_background_image();
        $this->doRespondSuccess($result);


    } 
    
    function delete_walk() {

        $result = array();
        $walk_id = $this->input->post('walk_id');
        $this->walk_model->delete_walk($walk_id);
        $this->doRespondSuccess($result);
    }
}

?>
